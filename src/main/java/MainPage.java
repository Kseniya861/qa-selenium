import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class MainPage {

    private WebDriver driver;
    private static final String URL = "http://13.48.31.160:8080/index";
    private static final String SWAGGER_URL = "http://13.48.31.160:8080/swagger-ui.html#";

    private static final By FIELD_NAME = By.xpath("//input[@id='name']");

    private static final By FIELD_SURNAME = By.xpath("//input[@id='surname']");

    private static final By CHECK_BOX = By.xpath("//label[@for='studentStep']");

    private static final By FIELD_GROUP = By.xpath("//input[@id='numberGroup']");

    private static final By SELECT_DAY = By.xpath("//select[@id='inputStateDay']");

    private static final By SELECT_TIME = By.xpath("//select[@id='inputStateTime']");

    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(URL);
    }

    public boolean atPage() {
        return driver.getCurrentUrl().equals(URL);
    }

    public boolean isConsultationModalOpen() {
        WebElement modal = driver.findElement(By.xpath("//div[@id='modalConsult']"));
        String classes = modal.getAttribute("class");
        return classes.contains("show");
    }

    public void openConsultationModal() {
        WebElement link = driver.findElement(By.xpath("//ul[@id='myMenu']/li/a[contains(text(), 'Консультация')]"));
        link.click();
    }

    public void fillConsultationForm(String name, String surname, boolean isStudent, String group) throws  Exception {
        driver.findElement(FIELD_NAME).sendKeys(name);

       driver.findElement(FIELD_SURNAME).sendKeys(surname);

       if(isStudent) {
           driver.findElement(CHECK_BOX).click();
           driver.findElement(FIELD_GROUP).sendKeys(group);
       }

       WebElement elementDay = driver.findElement(SELECT_DAY);
        Select selectDay = new Select(elementDay);
        selectDay.selectByIndex(1);

        Thread.sleep(1000);

       WebElement elementTime = driver.findElement(SELECT_TIME);
       Select selectTime = new Select(elementTime);
       selectTime.selectByIndex(0);
    }
}
