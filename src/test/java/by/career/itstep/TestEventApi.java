package by.career.itstep;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

import static java.util.UUID.randomUUID;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;

public class TestEventApi {

    private static final String BASE_URL = "http://13.48.31.160:8080/api/v1";
    private static final String STATUS_PUBLISHED = "PUBLISHED";

    @Test
    public void testFindAllEvents_happyPath() {
        int page = 0;
        int size = 3;

        RestAssured.baseURI = BASE_URL;
        String path = String.format("events?page=%s&size=%s", page, size);

        RequestSpecification req = RestAssured.given();
        Response response = req.request(Method.GET, path);

        String jsonBody = response.getBody().asString();
        int code = response.getStatusCode();

        System.out.println("Body: " + jsonBody);
        System.out.println("Code: " + code);

        Assert.assertEquals(SC_OK, code);
    }

    @Test
    public void testFindEventById_happyPath() {
        String id = "476a69ea-73a6-4803-a631-f3d2aec0b8bd";

        RestAssured.baseURI = BASE_URL;
        String path = String.format("events/%s", id);

        RequestSpecification req = RestAssured.given();
        Response response = req.request(Method.GET, path);

        String jsonBody = response.getBody().asString();
        int code = response.getStatusCode();
        String status = response.getBody().jsonPath().get("status");


        System.out.println("Body: " + jsonBody);
        System.out.println("Code: " + code);
        System.out.println("Event status: " + status);

        Assert.assertEquals(SC_OK, code);
        Assert.assertEquals(status, STATUS_PUBLISHED);
    }

    @Test
    public void testFindEventByIdIfExists() {
        String wrongId = randomUUID().toString();

        RestAssured.baseURI = BASE_URL;
        String path = String.format("events/%s", wrongId);

        RequestSpecification req = RestAssured.given();
        Response response = req.request(Method.GET, path);

        String jsonBody = response.getBody().asString();
        int code = response.getStatusCode();


        System.out.println("Body: " + jsonBody);
        System.out.println("Code: " + code);

        Assert.assertEquals(code, SC_NOT_FOUND);
    }
}
