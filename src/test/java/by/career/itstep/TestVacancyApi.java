package by.career.itstep;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_CREATED;

public class TestVacancyApi {

    private static final String EXP_INTERN = "INTERN";
    private static final String EXP_JUNIOR = "JUNIOR";
    private static final String EXP_MIDDLE = "MIDDLE";
    private static final String EXP_SENIOR = "SENIOR";

    private static final String BASE_URL = "http://13.48.31.160:8080/api/v1";

    @Test
    public void testCreateVacancy_happyPath() {
        RestAssured.baseURI = BASE_URL;
        String path = "/vacancies";

        RequestSpecification req = RestAssured.given();

        JSONObject json = new JSONObject();
        json.put("company", "IT-step");
        json.put("description", "Cool job description");
        json.put("experience", EXP_JUNIOR);
        json.put("name", "Developer");

        req.header("Content-Type", "application/json");
        req.body(json.toJSONString());  //Добавляем JSON в виде строки!

        Response response = req.request(Method.POST, path);
        String body = response.getBody().asString();
        int code = response.getStatusCode();

        System.out.println("Body: " + body);
        System.out.println("Code: " + code);

        Assert.assertEquals(code, SC_CREATED);
    }

    @Test
    public void testCreateVacancyIfNotValid() {
        RestAssured.baseURI = BASE_URL;
        String path = "/vacancies";

        RequestSpecification req = RestAssured.given();

        JSONObject json = new JSONObject();
        json.put("company", ""); //НЕ ВАЛИДНОЕ ИМЯ
        json.put("description", "Cool job description");
        json.put("experience", EXP_JUNIOR);
        json.put("name", "Developer");

        req.header("Content-Type", "application/json");
        req.body(json.toJSONString());  //Добавляем JSON в виде строки!

        Response response = req.request(Method.POST, path);
        String body = response.getBody().asString();
        int code = response.getStatusCode();

        System.out.println("Body: " + body);
        System.out.println("Code: " + code);

        Assert.assertEquals(code, SC_BAD_REQUEST);
    }

//    @Test
//    public void testFindVacancyById_happyPath() {
//        String path = "/vacancies";
//        String id = "e886224f-370f-4ab7-969b-a653cc525535";
//        Response response = RestAssured.given().when().get(path)
//                .getBody()
//                .jsonPath().get("key");
//    }
}
